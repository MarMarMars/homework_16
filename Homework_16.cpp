﻿#include <iostream>
#include <ctime>

int getDay() {

    struct tm newTime;
    time_t  currentDate = time(0);
    localtime_s(&newTime, &currentDate);

    return newTime.tm_mday;
}

int main()
{

    const int N = 6;

    int arr[N][N], day = getDay(), ost = 0, sum = 0;

    for (size_t i = 0; i < N; i++)
    {
        for (size_t j = 0; j < N; j++) {
            arr[i][j] = i + j;
            std::cout << " " << arr[i][j] << " ";
        }
        std::cout << "\n";
    }

    std::cout << "Current day of the month " << day << "\n";

    ost = (day % N) - 1;


    for (size_t j = 0; j < N; j++)
    {
        sum = sum + arr[ost][j];
    }

    std::cout << "Line amount " << ost << " = "  << sum;

}
